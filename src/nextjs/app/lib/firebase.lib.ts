import * as firebase from 'firebase/app'
import '@firebase/auth';
import '@firebase/firestore';
import { firebaseConfig } from '../webconfig.json';

firebase.apps.length || firebase.initializeApp(firebaseConfig);

// console.log('firebase.apps', Object.keys(firebase));
// !firebase.apps.length || firebase.initializeApp(firebaseConfig);
// console.log('firebaseConfig', firebaseConfig);
export default firebase;
