import { Button, Icon, Progress } from 'antd'
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import Questions from '../components/Questions'
import SelectableGender from '../components/SelectableGender'

const HeaderContainer = styled.div`
    display: flex;
    height: 350px;
    background: #5b9ebf;
    background: linear-gradient(to right, #8E55EA, #576FE7);
    background-position: center;
    position: relative;
    overflow: hidden;
`

const SvgContainer = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;

    svg {
        display: block;

        path {
            fill: #fff;
        }
    }
`

const TextContainer = styled.div`
    display: flex;
    color: #fff;
    flex-direction: column;
    align-items: center;
    width: 100%;
`

const TextHeader = styled.p`
    position: relative;
    top: 50px;
    margin-bottom: 0;

    & :first-child {
        font-size: 3rem;
    }
    & :last-child {
        font-size: 1.5rem;
    }
`

const TestDescription = styled.div`
    text-align: center;
`

const DescriptionContainer = styled.div`
    display: flex;
    justify-content: center;
`

const ContentContainer = styled.div`
    margin: 0 auto;
    height: 50px;
    background-color: #f9f9f9;
    display: flex;
    align-items: center;
    margin: 50px 0;
`

const NextButton = styled(Button)`
    margin: 0 auto;
    background-color: #6b63ff;
    border-color: #6b63ff;
    margin: 50px;

    & :hover {
        background-color: #6b63ff;
        border-color: #6b63ff;
    }

    & :focus {
        background-color: #6b63ff;
        border-color: #6b63ff;
    }
`

const CustomProgress = styled(Progress)`
    width: 70vw;
    margin: 0 auto;
    .ant-progress-inner {
        background-color: #f0f0f0;
        box-shadow: inset 0 0 2px #cccccc;
    }
    .ant-progress-bg {
        background-color: #6b63ff;
    }
`

const GenderContainer = styled.div`
    width: 60vw;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`

const RenderSection = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    line-height: 5;
`

class Test extends Component<any, any> {
    static getInitialProps({ reduxStore, req }) {
        return {}
    }

    constructor(props) {
        super(props)

        this.state = {
            error: false,
            counter: 0,
            questionId: 1,
            question: '',
            answerOptions: [],
            answersCount: {
                Introvert: 0,
                Extravert: 0,
                Intuition: 0,
                Sensing: 0,
                Thinking: 0,
                Feeling: 0,
                Judging: 0,
                Perceiving: 0,
            },
            answers: [
                {
                    id: '1',
                    value: '',
                },
            ],
            result: '',
            activeGender: '',
            gender: ['ผู้หญิง', 'ผู้ชาย', 'ไม่ระบุ'],
            ageRange: ['20 ปี', '20-30 ปี', '30 ปี ขึ้นไป'],
            activeAgeRange: '',
        }

        this.answerSelected = this.answerSelected.bind(this)
    }

    componentDidMount() {
        // DISPATCH ACTIONS HERE FROM `mapDispatchToProps`
        // this.timer = setInterval(() => this.props.startClock(), 1000)
    }

    componentWillMount() {
        const { questions } = this.props
        const AnswerOptions = questions.map(question => question.answers)
        this.setState({
            question: this.props.questions[0].question,
            answerOptions: AnswerOptions[0],
        })
    }

    answerSelected(event) {
        const selectedAnswer = this.state.answers.map(answer => {
            if (answer.id === event.target.name) {
                answer.id = event.target.name
                answer.value = event.target.value
                return false
            }
        })

        const answers = selectedAnswer.filter(answer => answer === false)
        answers[0] = answers[0] === undefined

        if (answers[0]) {
            this.state.answers.push({
                id: event.target.id,
                value: event.target.value,
            })
        }
    }

    userAnswer() {
        this.state.answers.map(answer => {
            const answerCount = answer.value
            const answ = this.state.answersCount[answerCount] + 1
            this.state.answersCount[answerCount] = answ
        })

        const zeroAnswers = [
            {
                id: '1',
                value: '',
            },
        ]

        this.setState({
            answersCount: this.state.answersCount,
            answers: zeroAnswers,
        })
    }

    nextPage = e => {
        e.preventDefault()

        const inputs = document.getElementsByTagName('input')
        const counter = this.state.counter + 1
        const questionId = this.state.questionId + 1

        if (this.state.answers.length === 4) {
            this.userAnswer()

            for (let i = 0; i < inputs.length; i++) {
                inputs[i].checked = false
            }

            if (this.state.questionId < this.props.questions.length) {
                this.setState({
                    counter,
                    questionId,
                    question: this.props.questions[counter].question,
                    answerOptions: this.props.questions[counter].answers,
                    answer: '',
                })
            } else {
                this.setState({ questionId })
                this.setResult(this.getResult())
            }
        } else {
            console.log('ตอบไม่ครบ')
        }
    }

    getResult() {
        const answersCount = this.state.answersCount
        const answersCountKeys = Object.keys(answersCount)

        let ie = Math.max(
            this.state.answersCount.Introvert,
            this.state.answersCount.Extravert,
        )
        let is = Math.max(
            this.state.answersCount.Intuition,
            this.state.answersCount.Sensing,
        )
        let tf = Math.max(
            this.state.answersCount.Thinking,
            this.state.answersCount.Feeling,
        )
        let jp = Math.max(
            this.state.answersCount.Judging,
            this.state.answersCount.Perceiving,
        )

        const ieArr = answersCountKeys.slice(0, 2)
        const isArr = answersCountKeys.slice(2, 4)
        const tfArr = answersCountKeys.slice(4, 6)
        const jpArr = answersCountKeys.slice(6, 8)

        ie = ieArr.filter(key => answersCount[key] === ie)
        is = isArr.filter(key => answersCount[key] === is)
        tf = tfArr.filter(key => answersCount[key] === tf)
        jp = jpArr.filter(key => answersCount[key] === jp)

        return ie.concat(is, tf, jp)
    }

    setResult(results) {
        this.setState({
            result: results,
        })
    }

    updateActiveGender = activeGender => {
        this.setState({
            activeGender,
        })
    }

    updateActiveAgeRange = activeAgeRange => {
        this.setState({
            activeAgeRange,
        })
    }

    renderQuestions() {
        return this.state.answerOptions.map(answerOption => {
            return (
                <div className='question-wrapper' key={answerOption.id}>
                    <Questions
                        questions={answerOption}
                        answerSelected={this.answerSelected}
                    />
                </div>
            )
        })
    }

    renderGender() {
        return (
            <Fragment>
                <div>เลือกเพศของคุณ</div>
                <GenderContainer>
                    {this.state.gender.map(sex => (
                        <SelectableGender
                            key={Math.random()}
                            option={sex}
                            activeGender={this.state.activeGender}
                            updateActiveGender={this.updateActiveGender}
                        />
                    ))}
                </GenderContainer>

                <div>เลือกช่วงอายุของคุณ</div>
                <GenderContainer>
                    {this.state.ageRange.map(age => (
                        <SelectableGender
                            key={Math.random()}
                            option={age}
                            activeGender={this.state.activeAgeRange}
                            updateActiveGender={this.updateActiveAgeRange}
                        />
                    ))}
                </GenderContainer>
            </Fragment>
        )
    }

    renderResult() {
        return <Result testResult={this.state.result} />
    }

    render() {
        const rate = Math.floor(((this.state.questionId - 1) * 100) / this.props.questions.length)

        return (
            <Fragment>
                <HeaderContainer>
                    <TextContainer>
                        <TextHeader>แบบทดสอบความเป็นตัวคุณ</TextHeader>
                        <TextHeader>NERIS Type Explorer®</TextHeader>
                    </TextContainer>
                    <SvgContainer>
                        <svg
                            xmlns='http://www.w3.org/2000/svg'
                            viewBox='0 0 1920 495'
                        >
                            <path
                                id='curve0'
                                d='M0,199V33S190,153.92,499.5,153.92C990.58,153.92,1065,0,1427.25,0,1712.37,0,1887,61.28,1920,79.14V196Z'
                                transform='translate(0, 305)'
                            />
                        </svg>
                    </SvgContainer>
                </HeaderContainer>
                <DescriptionContainer>
                    <TestDescription>
                        <div>แบบทดสอบทั้งหมด 40 ข้อ</div>
                        <div>ประมาณ 5 - 15 นาที</div>
                        <div>
                            เพื่อให้ได้ผลลัพธ์ที่ละเอียดที่สุด
                            พยายามอย่าตอบแบบ "กลางๆ"
                        </div>
                    </TestDescription>
                </DescriptionContainer>

                <ContentContainer>
                    <CustomProgress percent={rate} />
                </ContentContainer>

                <div>
                    <RenderSection>
                        <strong>
                            {this.state.result
                                ? 'Your results:'
                                : this.state.question}
                        </strong>

                        {this.state.result
                            ? this.renderGender()
                            : this.renderQuestions()}

                        <NextButton
                            type='primary'
                            size='large'
                            onClick={this.nextPage}
                        >
                            หน้าต่อไป
                            <Icon type='right' />
                        </NextButton>
                    </RenderSection>
                </div>
            </Fragment>
        )
    }
}

function mapStateToProps(state: any) {
    const questions = state.get('questions')
    return { questions }
}

export default connect(mapStateToProps)(Test)
