import { Footer } from '@app/components/footer';
import { AppLayout } from '@app/layouts/app-layout';
// import Head from 'next/head';
import { Layout } from 'antd';
import Link from 'next/link';
import React from 'react';
import styled from 'styled-components';
// import firebase from '../lib/firebase.lib';

let ui = undefined;
// import ui from 'firebaseui';
import { setUser } from '../actions/index';
import { bindActionCreators } from 'redux';
// import Router from 'next/router';
import { connect } from 'react-redux';
import { requestQuiz } from '../actions';
import questionsAnswers from '@app/reducers/questionsAnswers';
const Root = styled(Layout)`
  margin: 0 auto;
  padding-top: 70px;
`;

class IndexPage extends React.Component<any, any> {
  static async getInitialProps(context) {
    // console.log('##### CONTEXT #####', context);
    // context.store.dispatch(requestQuiz())
    const { isServer } = context;
    return { isServer };
  }

  render() {
    console.log('index.props', this.props);
    return (
      <AppLayout transparent sticky>
        <Root>
          <h1>Personality Test</h1>
          <Link href={`/quiz`} prefetch={true}>
            <button type="button">
              {/* {this.props.questionsLoaded?"Start Quiz":"Loading questions"} */}
              Start Quiz
            </button>
          </Link>
        </Root>
        <Footer />
      </AppLayout>
    );
  }
}

const mapStateToProps = state => {
  return {
    // questionsLoaded: state.quiz.questions.length > 0
  };
};

export default connect(mapStateToProps)(IndexPage);
