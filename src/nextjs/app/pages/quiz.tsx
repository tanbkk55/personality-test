import { Footer } from '@app/components/footer';
import { AppLayout } from '@app/layouts/app-layout';
import { Button, Layout } from 'antd';
import Link from 'next/link';
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { toJSON } from '../lib/utils';
// import { fromJS } from "immutable";
import Router from 'next/router';
import {
  requestQuiz,
  setQuizAnswer,
  setAptitudeSelection,
  setInterestSelection,
  setScoreAndWeight
} from '../actions/';
import Questions from '../components/Questions';
import firebase from '../lib/firebase.lib';

// import Router from "next/router";
// import questionsAnswers from "@app/reducers/questionsAnswers";
import { receiveQuiz } from '../actions/index';

const Root = styled(Layout)`
  margin: 0 auto;
  padding-top: 70px;
`;
class QuizPage extends React.Component<any, any> {
  static async getInitialProps(context) {
    // await context.store.dispatch(requestQuiz());
    let data = [];
    let snapshot = await firebase
      .app()
      .firestore()
      .collection('questionGroup')
      .where('is_active', '==', true)
      .get();
    snapshot.forEach(doc => {
      // console.log("doc", doc.data());
      data.push({ ...doc.data(), id: doc.id });
    });
    console.log('questionGroup', data[0]);
    context.store.dispatch(receiveQuiz(data[0]));
    snapshot = await firebase
      .app()
      .firestore()
      .collection('questionScoreAndWeight')
      .get();
    data = [];
    snapshot.forEach(doc => {
      data.push(doc.data());
    });
    console.log('scoreAndWeight', data);
    context.store.dispatch(setScoreAndWeight(data));
    // console.log(data);
    return { isServer: context.isServer };
  }

  state = {
    categoryIndex: 0,
    page: 0,
    perPage: 5,
    thisCategory: {
      category_id: '',
      category_name: ''
    },
    start: 0,
    end: 0,
    questions: [],
    questionsInCat: [],
    questionsObj: [],
    scoreAndWeightObj: [],
    answersObj: {},
    categoriesObj: []
  };

  constructor(props: any) {
    super(props);
    const questionsObj = props.questionsObj;
    const categoriesObj = props.categoriesObj;
    const scoreAndWeightObj = props.scoreAndWeightObj;
    const answersObj = props.answersObj;

    this.state = {
      categoryIndex: 0,
      page: 0,
      perPage: 5,
      thisCategory: categoriesObj[0],
      start: 0,
      end: 0,
      questions: [],
      questionsInCat: [],
      questionsObj,
      scoreAndWeightObj,
      categoriesObj,
      answersObj
    };
    // console.log('constructor state', this.state);
    // this.setState({...this.state, ...newState})
  }

  componentDidMount() {
    // if (this.props.isServer) {
    //   Router.push('/');
    //   return;
    // }
    // const questionsObj = JSON.parse(this.props.questionsObj);
    // const categoriesObj = JSON.parse(this.props.categoriesObj);
    // const scoreAndWeightObj = JSON.parse(this.props.scoreAndWeightObj);
    // const answersObj = JSON.parse(this.props.answersObj);
    // const newState = {
    //   categoryIndex: 0,
    //   page: 0,
    //   perPage: 5,
    //   thisCategory: categoriesObj[0],
    //   start: 0,
    //   end: 0,
    //   questions: { questions: [] },
    //   questionsInCat: [],
    //   questionsObj,
    //   scoreAndWeightObj,
    //   categoriesObj,
    //   answersObj
    // };
    // this.setState({ ...this.state, ...newState });
    // // this.props.requestQuiz();
    // console.log('componentDidMount props', this.props);
    // console.log('componentDidMount state', this.state);
    // console.log('questionObj.length', this.state.questionsObj.length);
    // if (this.props.categories) {
    // if (this.state.questionsObj.length > 0) {
    // this.setCategory(0);
    // }
    // }
  }

  setCategory(index) {
    // console.log('setCategory', this.state.categoriesObj);
    if (this.state.categoriesObj) {
      const thisCategory = this.state.categoriesObj[index];
      // console.log('thisCategory', thisCategory);
      const start = 0;
      const end = start + this.state.perPage;
      const questions = this.state.questionsObj.filter(q => {
        console.log('filter', q.category_id, thisCategory.category_id);
        return q.category_id === thisCategory.category_id;
      });
      // .slice(start, end);
      this.setState({
        ...this.state,
        thisCategory,
        categoryIndex: index,
        page: 0,
        questions: questions.slice(start, end),
        questionsInCat: questions,
        start,
        end
      });
    }
  }

  selectPage(index) {
    const start = index * this.state.perPage;
    const end = start + this.state.perPage;
    const nextState = {
      page: index,
      start,
      end,
      questions: this.props.questions
        .filter(q => {
          console.log(
            `q selectpage`,
            q.category_id,
            this.state.thisCategory.category_id
          );
          return q.category_id === this.state.thisCategory.category_id;
        })
        .slice(start, end)
    };
    console.log('nextState', nextState);
    this.setState({
      ...this.state,
      ...nextState
    });
  }

  renderQuestions(questions) {
    // return questions.map((q, i) => (
    //   <Fragment key={i}>
    //     <Questions
    //       key={`quiz-${q.question_id}-${i}`}
    //       text={q.text}
    //       question_id={q.question_id}
    //       question={q}
    //       scoreAndWeight={this.props.scoreAndWeight}
    //       // point_aptitude={this.props.point_aptitude}
    //       // point_interest={this.props.point_interest}
    //       // weight_aptitude={this.props.weight_aptitude}
    //       // weight_interest={this.props.weight_interest}
    //       answer={this.props.answers}
    //       raw_answer_aptitude={this.props.answers.raw_answer_aptitude}
    //       raw_answer_interest={this.props.answers.raw_answer_interest}
    //       selectAptitude={this.props.setAptitudeSelection}
    //       selectInterest={this.props.setInterestSelection}
    //     />
    //   </Fragment>
    // ));
  }

  renderQuestionsPage() {
    // console.log('renderQuestionsPage', this.state.thisCategory);
    return (
      <>
        {/* <div>{this.state.thisCategory.category_name}</div> */}
        {this.renderQuestions(this.state.questions)}
      </>
    );
  }

  gotAllAnswersInPage() {
    console.log('is checking');
    return this.state.questions.reduce((answered, q) => {
      console.log(
        `Checking`,
        q,
        this.props.answers.raw_answer_aptitude[q.question_id],
        this.props.answers.raw_answer_interest[q.question_id]
      );
      // return answered;
      return (
        answered &&
        !!this.props.answers.raw_answer_aptitude[q.question_id] &&
        !!this.props.answers.raw_answer_interest[q.question_id]
      );
    }, true);
  }

  renderActionButton() {
    if (this.props.categories && this.state.thisCategory) {
      let isAnsweredAll = this.gotAllAnswersInPage();
      const isLastCategories =
        this.props.categories[this.props.categories.length - 1].category_id ===
        this.state.thisCategory.category_id;
      const isLastPageInCategory =
        this.state.start + this.state.questions.length ===
        this.state.questionsInCat.length;

      console.log(
        'renderActionButton',
        isAnsweredAll,
        isLastCategories,
        isLastPageInCategory
      );
      if (isAnsweredAll && isLastCategories && isLastPageInCategory) {
        return (
          <Link href="/login">
            <Button>Login to see result</Button>
          </Link>
        );
      }

      if (isAnsweredAll && isLastPageInCategory) {
        return (
          <Button
            onClick={() => {
              this.setCategory(this.state.categoryIndex + 1);
            }}
          >
            Next Category
          </Button>
        );
      }
      if (isAnsweredAll) {
        return (
          <Button
            onClick={() => {
              this.selectPage(this.state.page + 1);
              // this.setState({ ...this.state, page: this.state.page + 1 });
            }}
          >
            Next page
          </Button>
        );
      }
      return <Button disabled={true}>Please answer all questions</Button>;
    }
  }

  render() {
    console.log('quiz page props', this.props);
    console.log('quiz page state', this.state);
    return (
      <AppLayout transparent sticky>
        <Root>
          {this.renderQuestionsPage()}
          {this.renderActionButton()}
          {/* {this.renderQuestions()}
          {this.renderActionButton()}
          answer total {this.props.answers.size}/
          {this.props.quizQuestions.length} ={' '}
          {Math.floor(
            100 * (this.props.answers.size / this.props.quizQuestions.length)
          )}
          % */}
        </Root>
        <Footer />
      </AppLayout>
    );
  }
}

const mapStateToProps = state => {
  // const quiz = state.get("quiz");
  // const answers = state.get("answers");
  // const scoreAndWeight = state.get("scoreAndWeight");

  const { quiz, answers, scoreAndWeight } = state;
  console.log('quiz mapStateToProps', state);

  const states = {
    // questions: quiz.questions,
    // categories: quiz.category,
    // scoreAndWeight: scoreAndWeight,
    // point_aptitude: scoreAndWeight.point_aptitude,
    // point_interest: scoreAndWeight.point_interest,
    // weight_aptitude: scoreAndWeight.weight_aptitude,
    // weight_interest: scoreAndWeight.weight_interest,
    // answers: answers,
    quizObj: quiz,
    questionsObj: quiz.questions,
    categoriesObj: quiz.category,
    thisCategoryObj: quiz.category[0],
    scoreAndWeightObj: scoreAndWeight,
    answersObj: answers
    // point_aptitudeObj: toJSON(scoreAndWE)
  };
  // console.log('states', states);
  // console.log()
  return states;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      requestQuiz,
      setQuizAnswer,
      setAptitudeSelection,
      setInterestSelection
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuizPage);
// แบ่งหน้าตาม category จากการกดปุ่ม
