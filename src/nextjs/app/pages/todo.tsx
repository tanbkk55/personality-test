import Link from 'next/link'
import React from 'react'
import styled from 'styled-components'

const Heading = styled.h1`
  color: red;
`

const Todo = (props) => {
    return (
        <>
            <Heading>TODO List for the project:</Heading>
            <ul>
                <li>Define the scope: What will be possible?</li>
                <li>Do mockups: Design the look</li>
                <li>Create: Code the components</li>
            </ul>
            <div>
                <Link href='/'>
                    <a>Go To Home Page</a>
                </Link>
            </div>
        </>
    )
}

export default Todo