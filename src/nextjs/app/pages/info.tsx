import { Footer } from "@app/components/footer";
import { AppLayout } from "@app/layouts/app-layout";
import { Layout } from "antd";
import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";
import { ageRange, gender } from "../webconfig.json";
import {
  setAgeRange,
  setGender,
  saveResultToStore,
  setAnswerUser,
  setAnswerScores
} from "../actions/index";
import { Seq } from "immutable";
import Router from "next/router";

const Root = styled(Layout)`
  margin: 0 auto;
  padding-top: 70px;
`;
class InfoPage extends React.Component<any, any> {
  static async getInitialProps(context) {
    const { isServer } = context;
    // console.log('store', store.getState().get('quiz'));
    // await context.store.dispatch(requestQuiz());

    return { isServer };
  }

  state = {};

  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    if (this.props.isServer) {
      Router.push("/");
    }
  }

  renderGender() {
    return gender.map((g, index) => {
      return (
        <button
          key={index}
          onClick={() => {
            this.props.setGender(g.value);
          }}
        >
          {g.display} {this.props.gender === g.value ? "selected" : ""}
        </button>
      );
    });
  }

  renderAgeRange() {
    return ageRange.map((age, index) => {
      let text = "";
      if (age.min === 0) {
        text = `ต่ำกว่า ${age.max} ปี`;
      } else if (age.max === 0) {
        text = `ตั้งแต่ ${age.min} ปีขึ้นไป`;
      } else {
        text = `อายุ ${age.min} - ${age.max} ปี`;
      }
      return (
        <button
          key={index}
          onClick={() => {
            this.props.setAgeRange(age.min, age.max);
          }}
        >
          {text}{" "}
          {age.min === this.props.ageMin && age.max === this.props.ageMax
            ? "selected"
            : ""}
        </button>
      );
    });
  }

  renderButtonResult() {
    if (
      (this.props.ageMin !== 0 || this.props.ageMax !== 0) &&
      this.props.gender !== ""
    ) {
      return (
        <button
          onClick={() => {
            this.calculateResult();
          }}
        >
          view result
        </button>
      );
    }
  }

  getScoreWeightById(id) {
    return this.props.scoreAndWeight.filter(s => {
      return s.id === id;
    });
  }

  calculateResult() {
    // this.props.saveResultToStore(payload);
    // // go to login page
    // Router.push("/login");
    this.props.setAnswerUser(
      this.props.userId,
      this.props.gender,
      this.props.ageMin,
      this.props.ageMax
    );

    // calculate
    // const Weight = this.getScoreWeightById
  }

  render() {
    return (
      <AppLayout transparent sticky>
        <Root>
          <div>
            <label>โปรดระบุข้อมูลของท่าน</label>
          </div>
          <div>
            <label>เลือกเพศของคุณ</label>
            {this.renderGender()}
          </div>
          <div>
            <label>เลือกช่วงอายุของคุณ</label>
            {this.renderAgeRange()}
          </div>
          <div>{this.renderButtonResult()}</div>
        </Root>
        <Footer />
      </AppLayout>
    );
  }
}

const mapStateToProps = state => {
  // const keys = state.get('quiz');
  // const quizes = state.get('quiz').get();
  // const quiz = state.get('quiz');
  const user = state.get("user");
  const quiz = state.get("quiz");
  const answers = state.get("answers");
  const scoreAndWeight = state.get("scoreAndWeight");

  return {
    userId: user.get("id"),
    userName: user.get("name"),
    userEmail: user.get("email"),
    userPicture: user.get("picture"),
    ageMin: user.get("ageMin"),
    ageMax: user.get("ageMax"),
    gender: user.get("gender"),
    quiz: quiz,
    answers: answers,
    scoreAndWeight: scoreAndWeight

    // username: 'aa',
    // quiz: quiz,
    // answers: quiz.answers || '{}'
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      // requestQuiz,
      // setQuizAnswer
      setAgeRange,
      setGender,
      saveResultToStore,
      setAnswerUser,
      setAnswerScores
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InfoPage);
