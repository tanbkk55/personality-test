// rename from index.tsx
import { Footer } from "@app/components/footer";
import { AppLayout } from "@app/layouts/app-layout";
import Head from "next/head";
import { Layout } from "antd";
// import Link from 'next/link';
import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import firebase from "../lib/firebase.lib";

let ui = undefined;
// import ui from 'firebaseui';
import { setUser, saveResult } from "../actions/index";
import { bindActionCreators } from "redux";
import Router from "next/router";
const Root = styled(Layout)`
  margin: 0 auto;
  padding-top: 70px;
`;

class IndexPage extends React.Component<any, any> {
  static async getInitialProps(context) {
    // console.log('##### CONTEXT #####', context);
    const { isServer } = context;
    return { isServer };
  }

  componentDidMount() {
    if (this.props.isServer) {
      Router.push("/");
    }

    const firebaseui = require("firebaseui");
    const thisPage = this;
    if (!ui) {
      ui = new firebaseui.auth.AuthUI(firebase.auth());
      // console.log('componentDidMount');
      var uiConfig = {
        callbacks: {
          signInSuccessWithAuthResult: function(authResult, redirectUrl) {
            const {
              email,
              name,
              picture,
              id
            } = authResult.additionalUserInfo.profile;
            // console.log('SETTING USER',email,name,picture,id,)
            thisPage.props.setUser(id, email, name, picture);
            // save here
            // thisPage.props.saveResult();
            return false;
          },
          uiShown: function() {
            // The widget is rendered.
            // Hide the loader.
            document.getElementById("loader").style.display = "none";
          }
        },
        // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
        signInFlow: "popup",
        // signInSuccessUrl: 'http://localhost:3000',
        signInOptions: [
          // Leave the lines as is for the providers you want to offer your users.
          firebase.auth.GoogleAuthProvider.PROVIDER_ID
        ]
        // Terms of service url.
        // tosUrl: '<your-tos-url>',
        // Privacy policy url.
        // privacyPolicyUrl: '<your-privacy-policy-url>'
      };
      ui.start("#firebaseui-auth-container", uiConfig);
    }
  }

  // componentDidUpdate() {
  //   console.log('#### D I D   U P D A T E', this.props);
  //   if (this.props.quizId && this.props.id) {
  //     alert('save result');
  //     // after login save the result
  //     this.props.saveResult();
  //   }
  // }

  renderLogin() {
    return (
      <div>
        <h2>กรุณาเข้าระบบ</h2>
        <div id="firebaseui-auth-container" />
        <div id="loader">Loading</div>
      </div>
    );
  }

  renderNextPage() {
    return (
      <div>
        <div>
          <img src={this.props.picture} />
        </div>
        <div>{this.props.name}</div>
        <div>{this.props.email}</div>
      </div>
    );
  }
  render() {
    // console.log('this.props', this.props);
    return (
      <AppLayout transparent sticky>
        <Head>
          <script src="https://cdn.firebase.com/libs/firebaseui/4.0.0/firebaseui.js" />
          <link
            type="text/css"
            rel="stylesheet"
            href="https://cdn.firebase.com/libs/firebaseui/4.0.0/firebaseui.css"
          />
        </Head>
        <Root>
          <h1>Personality Test</h1>
          {this.props.id ? this.renderNextPage() : this.renderLogin()}
        </Root>
        <Footer />
      </AppLayout>
    );
  }
}

const mapStateToProps = state => {
  const user = state.get("user").toJS();
  // console.log('mapStateToProps', user);
  const quiz = state.get("quiz").toJS();
  console.log("quiz", quiz);
  const result = state.get("result").toJS();
  console.log("result", result);
  const newState = {
    name: user.name,
    id: user.id,
    email: user.email,
    picture: user.picture,
    quizId: quiz.id
  };
  console.log("**** N E W   S T A T E ****", newState);
  return newState;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setUser,
      saveResult
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndexPage);
