import { Footer } from '@app/components/footer';
import { AppLayout } from '@app/layouts/app-layout';
import { Layout } from 'antd';
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { loadResult } from '../actions';
// import firebase from '../lib/firebase.lib';

// import { ageRange, gender } from '../webconfig.json';
// import { setAgeRange, setGender, saveResult } from '../actions/index';

const Root = styled(Layout)`
  margin: 0 auto;
  padding-top: 70px;
`;

class ResultPage extends React.Component<any, any> {
  static async getInitialProps(context) {
    // const router = useRouter();
    console.log(Object.keys(context.query));
    const query = Object.keys(context.query)[0];
    context.store.dispatch(loadResult(query));
    return { query };
  }

  render() {
    // console.log(this.props);
    // return <div>test{this.props.query}</div>;
    const { quizResult } = this.props;
    const skillSet = ['Analytic', 'Computation', 'Communication', 'Creative'];
    return (
      <AppLayout transparent sticky>
        <Root>
          <div>
            <label>Quiz Result</label>
          </div>
          <div>
            <div>Name: {this.props.name}</div>
            <div>email: {this.props.email}</div>
            <div>
              <img src={this.props.picture} />
            </div>
            <div>
              Age Range: {this.props.ageMin} - {this.props.ageMax}
            </div>
            <div>Gender: {this.props.gender}</div>
          </div>

          <div>
            {skillSet.map((skill, idx) => {
              const point = quizResult[`acc${skill}`];
              const totalPoint = quizResult[`total${skill}`];
              return (
                <div key={idx}>
                  {skill} = {point} of
                  {totalPoint} = {Math.floor(100 * (point / totalPoint))}%
                </div>
              );
            })}
          </div>
        </Root>
        <Footer />
      </AppLayout>
    );
    // const router = useRouter();
    // const { resultId } = router.query;
    // return <div>{resultId}</div>;
  }
}

const mapStateToProps = state => {
  const result = state.get('result').toJS();
  // console.log('store',state.get('result'))
  // console.log('result',result.toJS())
  return {
    name: result.name,
    email: result.email,
    picture: result.picture,
    ageMin: result.ageMin,
    ageMax: result.ageMax,
    gender: result.gender,
    quizResult: result.quizResult,
    quizId: result.quizId
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      loadResult
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResultPage);
