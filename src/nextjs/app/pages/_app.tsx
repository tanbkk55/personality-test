import '../../assets/antd-custom.less'

import { createTheme } from '@app/components/theme'
import { fromJS } from 'immutable'
import App, { Container } from 'next/app'
import React from 'react'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'styled-components'
import firebase from '../lib/firebase.lib';
import { getOrCreateStore } from '../store'
// import withRedux from 'next-redux-wrapper';
// import withReduxSaga from 'next-redux-saga';

import NProgress from 'nprogress'
import Router from 'next/router'

// console.log('_app.txt firebase', firebase);
Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

// tslint:disable-next-line: no-var-requires
// const firebaseConfig = require('../../config/fb-config.json')

// if (!firebase.apps.length) {
//     firebase.initializeApp(firebaseConfig)
// }

class CustomApp extends App {
    static async getInitialProps(appContext) {
        const { Component, ctx } = appContext
        const store = getOrCreateStore()
        ctx.store = store
        ctx.isServer = typeof window === 'undefined'
        console.log('_app.tsx is loaded')
        let pageProps = {}
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx)
        }

        return {
            pageProps,
            initialState: store.getState(),
        }
    }

    public store: any
    public theme: any

    constructor(props) {
        super(props)
        this.store = getOrCreateStore(fromJS(props.initialState))
        this.theme = createTheme()
    }

    render() {
        const { Component, pageProps } = this.props
        return (
            <Container>
                <ThemeProvider theme={this.theme}>
                    <Provider store={this.store}>
                        <Component {...pageProps} />
                    </Provider>
                </ThemeProvider>
            </Container>
        )
    }
}

export default CustomApp
// export default withRedux(getOrCreateStore)(withReduxSaga(CustomApp))