import {
  RECEIVE_QUIZ,
  REQUEST_QUIZ,
  SET_QUIZ_ANSWER,
  SET_USER,
  SET_AGE_RANGE,
  SET_GENDER,
  SAVE_RESULT_TO_STORE,
  SAVE_RESULT,
  GET_SAVE_RESULT,
  GET_LOAD_RESULT,
  LOAD_RESULT,
  SET_APTITUDE_SELECTION,
  SET_INTEREST_SELECTION,
  SET_ANSWER_USER,
  SET_ANSWER_SCORES,
  SET_SCORE_AND_WEIGHT
} from "./types";

export function selectType(type) {
  return {
    type: "TYPE_SELECTED",
    payload: type
  };
}

export function requestQuiz() {
  console.log("requestQuiz action");
  return {
    type: REQUEST_QUIZ
  };
}

export function receiveQuiz(payload) {
  console.log("receiveQuiz action");
  return {
    type: RECEIVE_QUIZ,
    payload
  };
}

export function setQuizAnswer(qid: Number, value: Number) {
  // console.log('set quiz answer #', qid)
  console.log(`Set quiz answer #${qid} with ${value}`);
  return {
    type: SET_QUIZ_ANSWER,
    payload: { qid, value }
  };
}

export function setUser(
  id: String,
  email: String,
  name: string,
  picture: String
) {
  return {
    type: SET_USER,
    payload: { id, email, name, picture }
  };
}

export function setAgeRange(min, max) {
  return {
    type: SET_AGE_RANGE,
    payload: { min, max }
  };
}

export function setGender(v) {
  return {
    type: SET_GENDER,
    payload: v
  };
}

export function saveResultToStore(payload) {
  return {
    type: SAVE_RESULT_TO_STORE,
    payload
  };
}

export function saveResult() {
  return {
    type: SAVE_RESULT
  };
}

export function getSaveResult(payload) {
  return {
    type: GET_SAVE_RESULT,
    payload
  };
}

export function loadResult(resultId) {
  return {
    type: LOAD_RESULT,
    payload: resultId
  };
}

export function getLoadResult() {
  return {
    type: GET_LOAD_RESULT
  };
}

export function setAptitudeSelection(qid, selection) {
  return {
    type: SET_APTITUDE_SELECTION,
    payload: { id: qid, value: selection }
  };
}

export function setInterestSelection(qid, selection) {
  return {
    type: SET_INTEREST_SELECTION,
    payload: { id: qid, value: selection }
  };
}

export function setAnswerUser(user_id, gender, age_min, age_max) {
  return {
    type: SET_ANSWER_USER,
    payload: {
      user_id,
      gender,
      age_min,
      age_max,
      age_string: `${age_min}-${age_max}`
    }
  };
}

export function setAnswerScores(
  question_group_id,
  skill_set_scores_aptitude,
  skill_set_scores_interest
) {
  return {
    type: SET_ANSWER_SCORES,
    payload: {
      question_group_id,
      skill_set_scores_aptitude,
      skill_set_scores_interest
    }
  };
}

export function setScoreAndWeight(data) {
  return {
    type: SET_SCORE_AND_WEIGHT,
    payload: data
  }
}
