const firebase = require("firebase/app");
// require('@firebase/auth');
require("@firebase/firestore");
const { firebaseConfig } = require("./webconfig.json");
const {
  questionGroup,
  questionScoreAndWeight
} = require("./initial-data.json");
firebase.apps.length || firebase.initializeApp(firebaseConfig);

// console.log(firebase);
// console.log(data);

questionGroup.questions = questionGroup.questions.map(q => {
  return {
    ...q,
    text: `Question id #${q.question_id}`,
    question_score_and_weight_id: "1"
  };
});

let promises = questionScoreAndWeight.map(ScoreAndWeight => {
  return firebase
    .app()
    .firestore()
    .collection("questionScoreAndWeight")
    .add(ScoreAndWeight);
});

promises.push(
  firebase
    .app()
    .firestore()
    .collection("questionGroup")
    .add(questionGroup)
);
Promise.all(promises).then(result => {
  result.forEach(r => {
    console.log(r.id);
  });
  process.exit();
});
