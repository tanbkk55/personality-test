import { Icon } from 'antd'
import React from 'react'
import styled from 'styled-components'

const CircleContainer = styled.div`
    width: 90px;
    height: 90px;
    border-radius: 50%;
    background: black;
    margin: 20px 0;
`

const SideContainer = styled.div`
    width: 350px;
    background: #F5F5F5;
    padding: 80px 0 0 30px;

`

const UsernameText = styled.div`
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-size: 24px;
    line-height: 23px;
    color: #000000;
`

const MenuItem = styled.div`
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 19px;
    color: #666666;
    margin: 10px 0;
`

const ItemContainer = styled.div`
    margin-top: 40px;
`

const ItemIcon = styled(Icon)`
    margin-right: 10px;
    font-size: 20px;
`

const ProfileSideMenu = (props) => {
    return (
        <SideContainer>
            <CircleContainer />
            <UsernameText>
                username
            </UsernameText>
            <ItemContainer>
                <MenuItem>
                    <ItemIcon type='user' />
                    ข้อมูลส่วนตัว
                </MenuItem>
                <MenuItem>
                    <ItemIcon type='file-text' />
                    รายการซื้อ
                </MenuItem>
            </ItemContainer>
        </SideContainer>
    )
}

export default ProfileSideMenu
