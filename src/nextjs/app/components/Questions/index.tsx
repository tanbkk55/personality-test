// import { style } from '@material-ui/system';
import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

const QuestionsContainer = styled.label`
  display: flex;
  flex-direction: row;
  width: 100vw;
  align-self: center;
  align-items: center;
  justify-content: center;
`;

const CheckedState = styled.div`
  display: flex;
  text-align: center;
  align-self: center;
  align-items: center;
  justify-content: center;

  input {
    display: none;
  }

  > input:checked + label {
    .option-circle {
      background-color: #34bc9b;
      color: #fff;
      box-shadow: 0 9px 18px rgba(0, 0, 0, 0.25),
        0 10px 10px rgba(0, 0, 0, 0.15);
      transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    }
  }
`;

const SelectLabel = styled.label`
  width: 100%;
  cursor: pointer;
  position: relative;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;

  .option-circle {
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    border: 3px solid #34bc9b;
    border-radius: 100%;
    background-color: #fff;
    width: 70px;
    height: 70px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 10px;
    color: #999;
    font-family: 'Lato', sans-serif;
    margin: 20px;
  }
`;

export default class Questions extends Component<any, any> {
  constructor(props: any) {
    super(props);
  }
  renderCheckState() {
    return (
      <Fragment>
        {this.props.scoreAndWeight
          // .toJS()
          .filter(
            s => s.id === this.props.question.question_score_and_weight_id
          )[0]
          .point_aptitude.map((p, i) => (
            <CheckedState key={`qa-${this.props.question_id}-${i}`}>
              <input
                id={`qa-${this.props.question_id}-${i}`}
                type="radio"
                name={`a-question-${this.props.question_id}`}
                value={p}
                checked={
                  this.props.answer.getIn([
                    'raw_answer_aptitude',
                    this.props.question_id
                  ]) === p
                }
                onChange={e => {
                  this.props.selectAptitude(
                    this.props.question_id,
                    parseFloat(e.target.value)
                  );
                }}
              />
              <SelectLabel htmlFor={`qa-${this.props.question_id}-${i}`}>
                <span className="option-circle" />
              </SelectLabel>
              {/* {`qa-${this.props.question_id}`} */}
            </CheckedState>
          ))}
        |||
        {this.props.scoreAndWeight
          // .toJS()
          .filter(
            s => s.id === this.props.question.question_score_and_weight_id
          )[0]
          .point_interest.map((p, i) => (
            <CheckedState key={`qi-${this.props.question_id}-${i}`}>
              <input
                id={`qi-${this.props.question_id}-${i}`}
                type="radio"
                name={`i-question-${this.props.question_id}`}
                value={p}
                checked={
                  this.props.answer.raw_answer_interest[
                    this.props.question_id
                  ] === p
                  // this.props.raw_answer_interest.get(this.props.question_id) === p
                }
                onChange={e => {
                  this.props.selectInterest(
                    this.props.question_id,
                    parseFloat(e.target.value)
                  );
                }}
              />
              <SelectLabel htmlFor={`qi-${this.props.question_id}-${i}`}>
                <span className="option-circle" />
              </SelectLabel>
              {/* {`qa-${this.props.question_id}`} */}
            </CheckedState>
          ))}
      </Fragment>
    );
  }

  render() {
    // console.log("Questions", this.props);
    return (
      <QuestionsContainer>
        <label>
          {this.props.text} <br />
          {this.props.question.category_id}
        </label>

        {this.renderCheckState()}
      </QuestionsContainer>
    );
  }
}
