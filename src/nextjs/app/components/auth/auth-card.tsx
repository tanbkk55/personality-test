import { Button, Card, Col, Divider, Row } from 'antd'
import React from 'react'
import styled from 'styled-components'
import { FacebookIcon } from '../icons/facebook-icon'
import { GoogleIcon } from '../icons/google-icon'

const Root = styled(Card)`
    width: 450px;
    margin-bottom: 30;
`

interface AuthCardProps {
    onGoogleClick: () => void
    onFacebookClick: () => void
}

export const AuthCard: React.SFC<AuthCardProps> = props => (
    <Root
        bodyStyle={{
            padding: 40,
        }}>
            <Row style={{ marginBottom: 20 }}>
                <Button size='large' block onClick={props.onFacebookClick}>
                    <FacebookIcon /> ดำเนินการต่อด้วย Facebook
                    </Button>
            </Row>

            <Row>
                <Button size='large' block onClick={props.onGoogleClick}>
                    <GoogleIcon /> ดำเนินการต่อด้วย Google
                    </Button>
            </Row>

            <Row style={{ marginBottom: 20, marginTop: 20 }}>
                <Divider>หรือ</Divider>
            </Row>

            {props.children}
    </Root>
)
