import { Icon } from 'antd'
import React, { Component, Fragment } from 'react'
import styled from 'styled-components'

const SelectableContainer = styled.div<any>`
    ${props =>
        props.isSelected &&
        `
        background-color: #e3fcec;
  `}
    position: relative;
    background-color: #fff;
    border: ${props =>
        props.isSelected ? '3px solid #6b63ff' : '3px solid #f9f9f9'};
    display: grid;
    width: 12rem;
    border-radius: .5rem;
    -webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, .12), 0 2px 4px 0 rgba(0, 0, 0, .08);
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, .12), 0 2px 4px 0 rgba(0, 0, 0, .08);
    padding: 1.5rem;

    &:hover {
        cursor: pointer;
        background-color: #f9f9f9
    }
`

const OptionContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-self: center;
    align-items: center;
`

const OptionText = styled.div`
    font-size: 1rem;
`

const IconStyle = styled(Icon)`
    font-size: 50px;
    display: flex;
    flex-direction: column;
    align-self: center;
    align-items: center;
`

const SvgContainer = styled.div`
    position: absolute;
    top: 0;
    right: 15px;
`

export default class SelectablePlan extends Component<any, any> {
    constructor(props: any) {
        super(props)
    }

    render() {
        const { option, updateActiveGender, activeGender } = this.props

        return (
            <Fragment>
                <SelectableContainer
                    isSelected={option === activeGender}
                    onClick={() => updateActiveGender(option)}
                >
                    {option === activeGender && (
                        <SvgContainer>
                            <svg
                                width='1.5rem'
                                height='1.5rem'
                                viewBox='0 0 200 200'
                                version='1.1'
                            >
                                <g
                                    id='Page-1'
                                    stroke='none'
                                    strokeWidth='1'
                                    fill='none'
                                    fillRule='evenodd'
                                >
                                    <g
                                        id='checkmark-outline'
                                        fillRule='nonzero'
                                    >
                                        <path
                                            d='M31.1442786,171.840796 C5.2779518,146.858262 -5.09578082,109.862896 4.01023318,75.0738981 C13.1162472,40.2848999 40.2848999,13.1162472 75.0738981,4.01023318 C109.862896,-5.09578082 146.858262,5.2779518 171.840796,31.1442786 C209.549474,70.1869539 209.010186,132.247241 170.628714,170.628714 C132.247241,209.010186 70.1869539,209.549474 31.1442786,171.840796 Z'
                                            id='Shape'
                                            fill='#D0CDFF'
                                        />
                                        <polygon
                                            id='Path'
                                            fill='#6b63ff'
                                            points='66.6666667 89.4527363 89.5522388 112.437811 132.338308 69.6517413 146.268657 83.7810945 89.5522388 140.298507 52.7363184 103.482587 66.6666667 89.3532338'
                                        />
                                    </g>
                                </g>
                            </svg>
                        </SvgContainer>
                    )}
                    <OptionContainer>
                        <div>
                            {option === 'ผู้หญิง' && (
                                <IconStyle type='user' />
                            )}
                            {option === 'ผู้ชาย' && (
                                <IconStyle type='user' />
                            )}
                        </div>
                        <OptionText>{option}</OptionText>
                    </OptionContainer>
                </SelectableContainer>
            </Fragment>
        )
    }
}
