import { Col, Layout, Row } from 'antd'
import Link from 'next/link'
import React from 'react'
import styled from 'styled-components'

const Root = styled(Layout.Footer)`
    border-top: 3px solid ${props => props.theme.primaryColor};
`

interface FooterProps {

}

const Footer: React.SFC<FooterProps> = props => {
    return (
        <Root>
            <Row gutter={20}>
                <Col span={6}>
                    <h3>personality</h3>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </p>
                </Col>
                <Col span={6}>
                    <h3>personality</h3>
                    <Link href='/about'>
                        <a>
                            <p>เกี่ยวกับเรา</p>
                        </a>
                    </Link>
                    <Link href='#'>
                        <a>
                            <p></p>
                        </a>
                    </Link>
                </Col>

                <Col span={6}>
                    <h3></h3>
                    <Link href='#'>
                        <a>
                            <p>personality</p>
                        </a>
                    </Link>
                    <Link href='#'>
                        <a>
                            <p>personality</p>
                        </a>
                    </Link>
                    <Link href='#'>
                        <a>
                            <p>personality</p>
                        </a>
                    </Link>
                </Col>

                <Col span={6}>
                    <h3>&nbsp;</h3>
                    <Link href='/teams-of-use'>
                        <a>
                            <p>ข้อกำหนดการใช้งาน</p>
                        </a>
                    </Link>
                    <Link href='/privacy'>
                        <a>
                            <p>นโยบายความเป็นส่วนตัวของเรา</p>
                        </a>
                    </Link>
                    <Link href='#'>
                        <a>
                            <p>บล็อก</p>
                        </a>
                    </Link>
                    <Link href='/help-center'>
                        <a>
                            <p>ศูนย์ช่วยเหลือ</p>
                        </a>
                    </Link>
                </Col>
            </Row>
        </Root>
    )
}

Footer.displayName = 'Footer'

export {
    Footer,
}
