// import { fromJS } from "immutable";
import moment from "moment";
import { SAVE_RESULT_TO_STORE, GET_LOAD_RESULT } from "../actions/types";

const INITIAL_STATE = {
  user_id: "",
  gender: "",
  age_max: 30,
  age_min: 20,
  age_string: "20-30",
  created_date: "",
  question_group_id: "",
  raw_answer_aptitude: {},
  raw_answer_interest: {},
  skill_set_scores_aptitude: {
    analytics: 0,
    communication: 0,
    computation: 0,
    creative: 0
  },
  skill_set_scores_interest: {
    analytics: 0,
    communication: 0,
    computation: 0,
    creative: 0
  }
};
export default function(state = INITIAL_STATE, action) {
  const { payload } = action;

  switch (action.type) {
    case SAVE_RESULT_TO_STORE:
      console.log("payload reducer", payload);
      // console.log(action.type, action.payload)
      // return fromJS({ ...state.toJS(), ...action.payload });
      // return state.set(action.payload.qid, action.payload.value);
      // return state
      //   .set("user_id", payload.user_id)
      //   .set("gender", payload.gender)
      //   .set("age_max", payload.age_max)
      //   .set("age_min", payload.age_min)
      //   .set("age_string", `${payload.age_min}-${payload.age_max}`)
      //   .set("created_date", moment().format("YYYY-MM-DD"))
      //   .set("question_group_id", payload.question_group_id)
      //   .set("raw_answer_aptitude", payload.raw_answer_aptitude)
      //   .set("raw_answer_interest", payload.raw_answer_interest)
      //   .set("skill_set_scores_aptitude", payload.skill_set_scores_aptitude)
      //   .set("skill_set_scores_interest", payload.skill_set_scores_interest);
      return {
        ...state,
        ...payload,
        age_string: `${payload.age_min}-${payload.age_max}`
      };

    case GET_LOAD_RESULT:
      console.log("**** P A Y L O A D ****", payload);
      // return state
      //   .set("user_id", payload.user_id)
      //   .set("gender", payload.gender)
      //   .set("age_max", payload.age_max)
      //   .set("age_min", payload.age_min)
      //   .set("age_string", `${payload.age_min}-${payload.age_max}`)
      //   .set("created_date", moment().format("YYYY-MM-DD"))
      //   .set("question_group_id", payload.question_group_id)
      //   .set("raw_answer_aptitude", payload.raw_answer_aptitude)
      //   .set("raw_answer_interest", payload.raw_answer_interest)
      //   .set("skill_set_scores_aptitude", payload.skill_set_scores_aptitude)
      //   .set("skill_set_scores_interest", payload.skill_set_scores_interest);
      return {
        ...state,
        ...payload,
        age_string: `${payload.age_min}-${payload.age_max}`
      }

    default:
      return state;
  }
}
