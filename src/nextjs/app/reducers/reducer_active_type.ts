import { fromJS } from 'immutable'

export default function(state = fromJS({}), action) {
  switch (action.type) {
    case 'TYPE_SELECTED':
      return action.payload
    default:
      return state
  }
}
