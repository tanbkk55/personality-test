// import { fromJS } from "immutable";
import {
  SET_QUIZ_ANSWER,
  SET_APTITUDE_SELECTION,
  SET_INTEREST_SELECTION,
  SET_ANSWER_USER,
  SET_ANSWER_SCORES
} from "../actions/types";
const INITIAL_STATE = {
  user_id: "",
  gender: "",
  age_max: 0,
  age_min: 0,
  age_string: "0",
  created_date: "",
  question_group_id: "",
  raw_answer_aptitude: {
    // "2SDFG43fd": 3,
    // ASD23232: 2
  },
  raw_answer_interest: {
    // "2SDFG43fd": 5,
    // ASD23232: 1
  },
  skill_set_scores_aptitude: {
    analytics: 0,
    communication: 0,
    computation: 0,
    creative: 0
  },
  skill_set_scores_interest: {
    analytics: 8,
    communication: 4,
    computation: 0,
    creative: 0
  }
};
export default function(state = INITIAL_STATE, action) {
  const { payload } = action;
  switch (action.type) {
    case SET_QUIZ_ANSWER:
      // return state.set(action.payload.qid, action.payload.value);
      return { ...state, [payload.qid]: payload.value };
    case SET_ANSWER_USER:
      return state;
    case SET_ANSWER_SCORES:
      return state;
    case SET_APTITUDE_SELECTION:
      // return state.setIn(["raw_answer_aptitude", payload.id], payload.value);
      return {
        ...state,
        raw_answer_apitude: {
          ...state.raw_answer_aptitude,
          [payload.id]: payload.value
        }
      };
    case SET_INTEREST_SELECTION:
      // return state.setIn(["raw_answer_interest", payload.id], payload.value);
      return {
        ...state,
        raw_answer_interest: {
          ...state.raw_answer_interest,
          [payload.id]: payload.value
        }
      };
    default:
      return state;
  }
}
