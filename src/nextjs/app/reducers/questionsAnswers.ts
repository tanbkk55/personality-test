export default function() {
  return [
    {
      question: 'คุณเป็นคนคิดเลขในใจได้อย่างรวดเร็ว',
      answers: [
        {
          id: 1,
          typeA: 'Introvert',
          contentA: 'Not like Me',
          typeB: 'Extravert',
          contentB: 'Alot like me',
        },
        {
          id: 2,
          typeA: 'Intuition',
          contentA: 'Not like Me',
          typeB: 'Sensing',
          contentB: 'Alot like me',
        },
        {
          id: 3,
          typeA: 'Thinking',
          contentA: 'Not like Me',
          typeB: 'Feeling',
          contentB: 'Alot like me',
        },
        {
          id: 4,
          typeA: 'Judging',
          contentA: 'Not like Me',
          typeB: 'Perceiving',
          contentB: 'Alot like me',
        },
      ],
    },
    {
      question: 'คุณเป็นคนที่ชอบจิตนาการถึงสิ่งที่ยังไม่เคยพบเจอมาก่อนหรือไม่',
      answers: [
        {
          id: 1,
          typeA: 'Introvert',
          contentA: 'Not like Me',
          typeB: 'Extravert',
          contentB: 'Alot like me',
        },
        {
          id: 2,
          typeA: 'Intuition',
          contentA: 'Not like Me',
          typeB: 'Sensing',
          contentB: 'Alot like me',
        },
        {
          id: 3,
          typeA: 'Thinking',
          contentA: 'Not like Me',
          typeB: 'Feeling',
          contentB: 'Alot like me',
        },
        {
          id: 4,
          typeA: 'Judging',
          contentA: 'Not like Me',
          typeB: 'Perceiving',
          contentB: 'Alot like me',
        },
      ],
    },
    {
      question: 'คุณเป็นคนที่เล่าจิตนาการของคุณให้กับผู้อื่นหรือไม่',
      answers: [
        {
          id: 1,
          typeA: 'Introvert',
          contentA: 'Not like Me',
          typeB: 'Extravert',
          contentB: 'Alot like me',
        },
        {
          id: 2,
          typeA: 'Intuition',
          contentA: 'Not like Me',
          typeB: 'Sensing',
          contentB: 'Alot like me',
        },
        {
          id: 3,
          typeA: 'Thinking',
          contentA: 'Not like Me',
          typeB: 'Feeling',
          contentB: 'Alot like me',
        },
        {
          id: 4,
          typeA: 'Judging',
          contentA: 'Not like Me',
          typeB: 'Perceiving',
          contentB: 'Alot like me',
        },
      ],
    },
    {
      question: 'คุณเป็นคนที่ไม่ค่อยสบายใจเมื่อต้องพูดต่อหน้าคนจำนวนมากใช่หรือไม่',
      answers: [
        {
          id: 1,
          typeA: 'Introvert',
          contentA: 'Not like Me',
          typeB: 'Extravert',
          contentB: 'Alot like me',
        },
        {
          id: 2,
          typeA: 'Intuition',
          contentA: 'Not like Me',
          typeB: 'Sensing',
          contentB: 'Alot like me',
        },
        {
          id: 3,
          typeA: 'Thinking',
          contentA: 'Not like Me',
          typeB: 'Feeling',
          contentB: 'Alot like me',
        },
        {
          id: 4,
          typeA: 'Judging',
          contentA: 'Not like Me',
          typeB: 'Perceiving',
          contentB: 'Alot like me',
        },
      ],
    },
    {
      question: 'คุณเป็นคนที่ชอบจิตนาการถึงสิ่งที่ยังไม่เคยพบเจอมาก่อนหรือไม่',
      answers: [
        {
          id: 1,
          typeA: 'Introvert',
          contentA: 'Not like Me',
          typeB: 'Extravert',
          contentB: 'Alot like me',
        },
        {
          id: 2,
          typeA: 'Intuition',
          contentA: 'Not like Me',
          typeB: 'Sensing',
          contentB: 'Alot like me',
        },
        {
          id: 3,
          typeA: 'Thinking',
          contentA: 'Not like Me',
          typeB: 'Feeling',
          contentB: 'Alot like me',
        },
        {
          id: 4,
          typeA: 'Judging',
          contentA: 'Not like Me',
          typeB: 'Perceiving',
          contentB: 'Alot like me',
        },
      ],
    },
  ]
}
