// import { fromJS } from "immutable";
import { RECEIVE_QUIZ, REQUEST_QUIZ } from '../actions/types';
const INITIAL_STATE = {
  id:"",
  created_date: '',
  is_active: true,
  questions: [],
  category: []
};
export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case REQUEST_QUIZ:
      return state;
    case RECEIVE_QUIZ:
      console.log('receive quiz', action);
      // return fromJS(action.payload);
      return action.payload;
    default:
      return state;
  }
}
