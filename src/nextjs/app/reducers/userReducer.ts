// import { fromJS } from 'immutable';
import { SET_USER, SET_AGE_RANGE, SET_GENDER } from "../actions/types";
const INITIAL_STATE = {
  name: "",
  email: "",
  picture: "",
  id: "",
  ageMin: 0,
  ageMax: 0,
  gender: ""
};
export default function(state = INITIAL_STATE, action) {
  const { payload } = action;
  switch (action.type) {
    case SET_USER:
      // return state
      //   .set('name', payload.name)
      //   .set('email', payload.email)
      //   .set('picture', payload.picture)
      //   .set('id', payload.id);
      return { ...state, ...payload };
    // return fromJS({ ...state.toJS(), ...action.payload });
    case SET_AGE_RANGE:
      // return state.set("ageMax", payload.max).set("ageMin", payload.min);
      return { ...state, ageMax: payload.max, ageMin: payload.min };
    // return fromJS({
    //   ...state.toJS(),
    //   ageMax: action.payload.max,
    //   ageMin: action.payload.min
    // });
    case SET_GENDER:
      // return state.set("gender", payload);
      return { ...state, gender: payload };
    // return fromJS({
    //   ...state.toJS(),
    //   gender: action.payload
    // })
    default:
      return state;
  }
}
