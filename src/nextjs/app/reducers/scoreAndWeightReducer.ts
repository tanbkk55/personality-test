// import { fromJS } from "immutable";
import { LOAD_QUESTION_SCORE } from "../actions/types";
const INITIAL_STATE = [
  {
    point_aptitude: [1, 2, 3, 4, 5],
    point_interest: [1, 2, 4, 6, 8],
    weight_aptitude: {
      analytics: 1,
      communication: 2,
      computation: 0,
      creative: -1
    },
    weight_interest: {
      analytics: 1,
      communication: 2,
      computation: 0,
      creative: -1
    }
  }
];
export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOAD_QUESTION_SCORE:
      const { payload } = action;
      // return state
      //   .set("point_aptitude", payload.point_aptitude)
      //   .set("point_interest", payload.point_interest)
      //   .set("weight_aptitude", payload.weight_aptitude)
      //   .set("weight:interest", payload.weight_interest);
      return { ...state, ...payload };
    default:
      return state;
  }
}
