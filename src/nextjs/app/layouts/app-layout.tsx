import { Layout } from 'antd'
import React from 'react'

interface AppLayoutProps {
    sticky?: boolean
    transparent?: boolean
}

class AppLayout extends React.Component<AppLayoutProps> {
    state = {
        collapsed: false,
    }

    handleToggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        })
    }

    render() {
        return (
            <Layout style={{ minHeight: '100vh' }}>
                {this.props.children}
            </Layout>
        )
    }
}

export {
    AppLayout,
}
