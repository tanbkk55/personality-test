import { Layout } from 'antd'
import React from 'react'

export class PortalLayout extends React.Component {
    render() {
        return (
            <Layout>
                {this.props.children}
            </Layout>
        )
    }
}
