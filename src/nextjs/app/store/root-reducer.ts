// import { combineReducers } from "redux-immutable";
import { combineReducers } from "redux";
// import ReducerQuestions from '../reducers/questionsAnswers'
// import ReducerActiveType from '../reducers/reducer_active_type'
// import ReducerTypes from '../reducers/reducer_types'
import UserReducer from "../reducers/userReducer";
import QuizReducer from "../reducers/quizReducer";
import AnswerReducer from "../reducers/answerReducer";
import ResultReducder from "../reducers/resultReducer";
import scoreAndWeightReducer from "../reducers/scoreAndWeightReducer";

export default () =>
  combineReducers({
    // types: ReducerTypes,
    // activeType: ReducerActiveType,
    // questions: ReducerQuestions,
    user: UserReducer,
    quiz: QuizReducer,
    answers: AnswerReducer,
    result: ResultReducder,
    scoreAndWeight: scoreAndWeightReducer
  });
