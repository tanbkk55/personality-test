// // @ts-check
import { all, put, takeEvery, call, select } from "redux-saga/effects";
// // import { fromJS } from "immutable";
// import Router from "next/router";
// import {
//   REQUEST_QUIZ,
//   RECEIVE_QUIZ,
//   // SAVE_RESULT,
//   // LOAD_QUESTION_SCORE,
//   GET_QUESTION_SCORE,
//   SET_USER,
//   LOAD_RESULT,
//   GET_LOAD_RESULT
// } from "../actions/types";
// // import firebase from 'firebase/app';
// // import '@firebase/auth';
// // import '@firebase/firestore';
// import firebase from "../lib/firebase.lib";


// // collections in firestore
// const questionGroup = "questionGroup";
// const questionScoreAndWeight = "questionScoreAndWeight";
// const testResult = "testResult";

// function firebaseGetQuiz() {
//   return firebase
//     .app()
//     .firestore()
//     .collection(questionGroup)
//     .where("is_active", "==", true)
//     .get()
//     .then(snapshot => {
//       // console.log('snapshot',snapshot)
//       let data = [];
//       snapshot.forEach(doc => {
//         // console.log("doc", doc.data());
//         data.push({ ...doc.data(), id: doc.id });
//       });
//       // console.log("data0", data[0]);
//       return { quizData: data[0] };
//     })
//     .catch(e => {
//       console.log("get quiz error", e);
//     });
// }

// function* firebaseGetScoreAndWeight() {
//   return firebase
//     .app()
//     .firestore()
//     .collection(questionScoreAndWeight)
//     .get()
//     .then(snapshot => {
//       let data = [];
//       snapshot.forEach(doc => {
//         data.push({ ...doc.data(), id: doc.id });
//       });
//       return { ScoreAndWeight: data };
//     });
// }

// function* firebaseCreateResultDoc(payload) {
//   return firebase
//     .app()
//     .firestore()
//     .collection(testResult)
//     .add(payload)
//     .then(ref => ref.id);
// }

// function* firebaseLoadResultDoc(payload) {
//   return firebase
//     .app()
//     .firestore()
//     .collection(testResult)
//     .doc(payload)
//     .get()
//     .then(snapshot => {
//       return snapshot;
//     });
// }

// export const getResultFromState = state => {
//   // console.log('getREsultFromState', state);
//   return state.get("result");
// };

// export const getUserFromState = state => {
//   return state.get("user");
// };

// export function* requestQuizSaga() {
//   // console.log('requestQuiz saga');
//   const response = yield call(firebaseGetQuiz);
//   // console.log('saga response', response);
//   // const result = yield response;
//   console.log('saga quiz result', response);
//   yield put({ type: RECEIVE_QUIZ, payload: response.quizData });
// }

// export function* watchQuestionSaga() {
//   // console.log('watchQuiz saga');
//   // return new Promise((resolve, reject) => {
//   yield takeEvery(REQUEST_QUIZ, requestQuizSaga);
//   // });
// }

// export function* saveResultSaga() {
//   // console.log('saveResultSaga', action);

//   const payload = yield select(getResultFromState);
//   // console.log("*** R E S U L T   F R O M   S T A T E ***", payload.toJS());
//   const user = yield select(getUserFromState);
//   // console.log("*** R E S U L T   F R O M   S T A T E ***", user.toJS());
//   const data = payload
//     .set("email", user.get("email"))
//     .set("name", user.get("name"))
//     .set("picture", user.get("picture"));
//   // console.log("*** R E S U L T   F R O M   S T A T E ***", data.toJS());
//   const response = yield call(firebaseCreateResultDoc, data.toJS());
//   const id = yield response;
//   // // console.log('###### R E D I R E C T ######');
//   yield call(Router.push, `/result?${id}`);
//   // // yield put({ type: LOAD_RESULT, payload: id });
// }

// export function* watchSaveResultSaga() {
//   // console.log('watchSaveResultSaga');
//   // yield takeEvery(SAVE_RESULT, saveResultSaga);
//   yield takeEvery(SET_USER, saveResultSaga);
// }

// export function* loadResultSaga(action) {
//   const response = yield firebaseLoadResultDoc(action.payload);
//   // console.log("***** S A G A   R E S P O N S E *****", response.data());
//   const data = yield response;

//   // console.log("***** S A G A *****", data.data());
//   yield put({
//     type: GET_LOAD_RESULT,
//     payload: data.data()
//   });
// }

// export function* watchLoadResultSaga() {
//   // console.log('watchLoadResult');
//   yield takeEvery(LOAD_RESULT, loadResultSaga);
// }

// export function* loadQuestionScore() {
//   const response = yield firebaseGetScoreAndWeight();
//   const data = yield response;
//   // console.log("loadQuestionScore", data);
//   yield put({
//     type: GET_QUESTION_SCORE,
//     payload: data
//   });
// }

export default function*() {
  yield all([
    // ...authModuleSaga,
    // watchQuestionSaga(),
    // requestQuizSaga(),
    // watchSaveResultSaga(),
    // watchLoadResultSaga(),
    // loadQuestionScore()
  ]);
}
