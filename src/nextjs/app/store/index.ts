// import { fromJS } from 'immutable';
import { applyMiddleware, createStore as createReduxStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import createReducer from './root-reducer';
import rootSaga from './root-saga';

const saga = createSagaMiddleware();
const middleware = [saga];
const isServer = typeof window === 'undefined';
const __NEXT_REDUX_STORE__ = '__NEXT_REDUX_STORE__';

let store = undefined;

if (process.env.NODE_ENV !== 'production') {
    // tslint:disable-next-line: no-var-requires
    const reduxLogger = require('redux-logger')
    const logger = reduxLogger.createLogger({
        collapsed: true,
        // stateTransformer: state => state.toJS(),

    })
    middleware.push(logger)
}

// export function createStore(initialState = fromJS({})) {
export function createStore(initialState = {}) {
    if (!store) {
    store = createReduxStore(
      createReducer(),
      initialState,
      applyMiddleware(...middleware)
    );
    saga.run(rootSaga);
    // console.log('run root saga');
  }
  return store;
}

// export function getOrCreateStore(initialState = fromJS({})) {
export function getOrCreateStore(initialState = {}) {
    // console.log('call getOrCreateStore');
  if (isServer) {
    return createStore(initialState);
  }

  if (!window[__NEXT_REDUX_STORE__]) {
    window[__NEXT_REDUX_STORE__] = createStore(initialState);
  }
  return window[__NEXT_REDUX_STORE__];
}
