const defaultConfig = require("../../jest.config");

module.exports = {
    rootDir: "../../",
    ...defaultConfig,
};
