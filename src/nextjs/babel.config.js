module.exports = function(api) {
    api.cache(true);
  
    const presets = [
      "next/babel",
      ["@babel/preset-typescript", { isTSX: true, allExtensions: true }]
    ];
  
    const plugins = [
        ['@babel/plugin-proposal-class-properties', { loose: true }],
        ['@babel/plugin-proposal-object-rest-spread', { loose: true }],
        ['module-resolver', {
            'root': ['.'],
            'alias': {
              '@app': './app',
            }
        }],
        ['styled-components', {
            'ssr': true,
            'displayName': true,
            'preprocess': false
        }]
    ];
  
    return {
      presets,
      plugins
    };
  };