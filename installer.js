var join = require("path").join;
var spawn = require("child_process").spawn;
var os = require("os");

// npm binary based on OS
var npmCmd = os.platform().startsWith("win") ? "npm.cmd" : "npm";

var arguments = process.argv.slice(3, process.argv.length);
// npmCmd = '/bin/echo'
spawn(`${npmCmd}`, ["i", ...arguments], {
  env: process.env,
  cwd: join(__dirname, "src", "nextjs"),
  stdio: "inherit"
});

spawn(`${npmCmd}`, ["i", ...arguments], {
  env: process.env,
  cwd: join(__dirname, "src", "functions"),
  stdio: "inherit"
});
